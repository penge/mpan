# MPAN

https://en.wikipedia.org/wiki/Meter_Point_Administration_Number


## Problem

Our goal is to read MPAN from PDF files of various energy suppliers.
We know there are many of them and they use different PDF styling.
Luckily, MPAN is 21-digit that can make it easier to find in PDF.


## Solution

Using `pdf-parse` to extract PDF text.
MPAN is then found using a simple regular expression.

Better solution is to store MPAN in file metadata.
Adding `blank.pdf` to prove MPAN can be read using `exiftool`.

Both options are implemented and found in `./strategies` folder.


## Use

**In code:**
```js
const mpan = require('./mpan');

// Read from PDF
const foundMpan = await mpan.read(filePath, strategy);

// Manual use
const identifiedMpan = mpan.identify('012345678901234567890');
```

**In console:**
```
# Basic use
$ node mpancli.js public/avro.pdf
$ node mpancli.js public/isupply.pdf
$ node mpancli.js public/octopus.pdf

# Explicit strategy
$ node mpancli.js public/avro.pdf parse
$ node mpancli.js public/blank.pdf meta

# Executable
$ ./mpancli.js public/avro.pdf

# Global
$ mpancli public/avro.pdf
```


## MPAN example

```js
{
  full: '012345678901234567890',
  top: {
    profileType: '01',
    meterTimeSwitchCode: '234',
    lineLossFactor: '567'
  },
  core: {
    distributorId: '89',
    uniqueIdentifier: '01234567',
    checkDigit: '890'
  }
}
```


## Tests

```
$ node test.js
```

Unit tests are not included.
Usually I use AVA to write them.


## Conclusion

The best solution is one where we don't need to parse PDF files.
If MPAN is the only information we need from PDF, parsing PDF may still work fine.
In case we need more kind of information, adding metadata to the file would be easiest way to achieve it. It requires little effort from suppliers and simplifies things greatly.

This project could be distributed as NPM package, making it reusable. In that case, example PDF files would be removed and `package.json` would be updated to point to the main file. Unit tests would be added also.

In case of questions, please contact me.
