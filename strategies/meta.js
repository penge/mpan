const exiftool = require('node-exiftool');
const ep = new exiftool.ExiftoolProcess();


module.exports = path => {
  return new Promise((resolve, reject) => {
    ep.open()
      .then(() => ep.readMetadata(path, ['Mpan']))
      .then(result => resolve(result.data[0].Mpan))
      .then(() => ep.close());
  });
};
