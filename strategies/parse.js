const fs = require('fs');
const pdf = require('pdf-parse');


// Any characters followed by " S " (S with spaces around),
// followed by a bunch of numbers (MPAN), and followed by any characters again.
const re = /.*\sS\s([\d\s]{21,}).*/;


module.exports = path => {
  return new Promise((resolve, reject) => {

    // Read file from drive (in real environment in can be S3)
    fs.readFile(path, (err, buffer) => {

      // Reject if file reading goes wrong
      if (err) {
        return reject(err);
      };

      // Turn PDF into text
      pdf(buffer).then(data => {

        // Look for MPAN
        const found = re.exec(data.text);
        let mpan = found && found[1];

        // Remove whitespaces
        mpan = mpan && mpan.split(' ').join('');

        // Return MPAN
        resolve(mpan);

      }).catch(err => {
        // Reject if turning PDF into text goes wrong
        reject(err);
      });
    });
  });
};
