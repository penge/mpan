/*
  Reads MPAN from every PDF file.
*/
const mpan = require('./mpan');
const test = async () => {
  try {
    const public = [
      { path: __dirname + '/public/avro.pdf' },
      { path: __dirname + '/public/isupply.pdf' },
      { path: __dirname + '/public/octopus.pdf' },
      { path: __dirname + '/public/blank.pdf', strategy: 'meta' }
    ];

    public.forEach(async ({ path, strategy }) => {
      let foundMpan = await mpan.read(path, strategy || 'parse');
      console.log(path);
      console.log('MPAN', foundMpan || 'Not found!');
      console.log();
    });

  } catch (err) {
    console.log(err);
  }
};

// Run Test
test();
