// Check MPAN to be 21-digit
const is21 = mpan => {
  return mpan && mpan.length === 21;
};



// Sections of MPAN
const getProfileType = mpan => mpan.substr(0, 2);
const getMeterTimeSwitchCode = mpan => mpan.substr(2, 3);
const getLineLossFactor = mpan => mpan.substr(5, 3);
const getDistributorId = mpan => mpan.substr(8, 2);
const getUniqueIdentifier = mpan => mpan.substr(10, 8);
const getCheckDigit = mpan => mpan.substr(18, 3);



// Complete MPAN
const identify = mpan => {
  if (!is21(mpan)) {
    return;
  };

  return {
    full: mpan,
    top: {
      profileType: getProfileType(mpan),
      meterTimeSwitchCode: getMeterTimeSwitchCode(mpan),
      lineLossFactor: getLineLossFactor(mpan),
    },
    core: {
      distributorId: getDistributorId(mpan),
      uniqueIdentifier: getUniqueIdentifier(mpan),
      checkDigit: getCheckDigit(mpan)
    }
  };
};



// Strategies:
// parse.js - turn PDF to text and look for MPAN
// meta.js  - read XMP metadata from PDF
const strategies = require('./strategies');



// Read MPAN from PDF (2 strategies supported)
const read = async (path, strategy = 'parse') => {
  let foundStrategy = strategies[strategy];
  if (!foundStrategy) {
    throw 'Stategy not found!';
  };

  // Read MPAN from file / or file metadata
  const mpan = await foundStrategy(path);

  // Return nice MPAN
  return identify(mpan);
};


module.exports = {
  identify,
  read
};
