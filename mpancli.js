#!/usr/bin/env node

/*
  Reads MPAN from PDF file.

  2 supported strategies, check ./strategies folder.

  Example of use:
  $ node mpancli.js public/avro.pdf          (parse strategy by default)
  $ node mpancli.js public/avro.pdf parse    (parse strategy explicitly)
  $ node mpancli.js public/blank.pdf meta    (meta strategy)

  Using as executable:
  $ ./mpancli.js public/avro.pdf

  Using globally:
  $ npm link
  $ mpancli public/avro.pdf
  $ npm unlink
*/


// Read arguments
const filePath = process.argv[2];
const strategy = process.argv[3] || 'parse';


// File path REQUIRED!
if (!filePath) {
  console.log('File path missing!');
  process.exit(1);
};



// APP
const mpan = require('./mpan');
const app = async() => {
  try {
    console.log('PATH', filePath);
    const foundMpan = await mpan.read(filePath, strategy);
    console.log('MPAN', foundMpan || 'Not found!');
  } catch (err) {
    console.log(err);
  }
};

// Run APP
app();
